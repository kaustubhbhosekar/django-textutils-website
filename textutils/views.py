"""
    user created file @kaustubh
"""

from django.http import HttpResponse
from django.shortcuts import render


def index(request):
    return render(request, 'index.html')


def analyze(request):
    text1 = request.POST.get('text1', 'Kindly enter text for analyzing')
    if text1 == '' or text1 is None:
        return HttpResponse("Kindly enter text for analyzing")
    params = {}

    removepunc = request.POST.get('removepunc', 'off')
    fullcaps = request.POST.get('fullcaps', 'off')
    newlineremover = request.POST.get('newlineremover', 'off')
    spaceremover = request.POST.get('spaceremover', 'off')
    charcount = request.POST.get('charcount', 'off')
    if removepunc == "on":
        punctuations = '''|!()-[]{}+=;:'"\,<>.?/@#$%^&*_~'''
        analyzed = ""
        for char in text1:
            if char not in punctuations:
                analyzed = analyzed + char

        params = {'purpose': 'Removed Punctuations', 'analyzed_text': analyzed}
        text1 = analyzed
    if fullcaps == "on":
        analyzed = ""
        for char in text1:
            analyzed = analyzed + char.upper()
        params = {'purpose': 'To Upper Case', 'analyzed_text': analyzed}
        text1 = analyzed
    if newlineremover == "on":
        analyzed = ""
        for char in text1:
            if char != "\n" and char != "\r":
                analyzed = analyzed + char
        params = {'purpose': 'Remove new line ', 'analyzed_text': analyzed}
        text1 = analyzed
    if spaceremover == "on":
        analyzed = ""
        for index, char in enumerate(text1):
            if not (text1[index] == " " and text1[index + 1] == " "):
                analyzed = analyzed + char

        params = {'purpose': 'Remove Space ', 'analyzed_text': analyzed}
        text1 = analyzed
    if charcount == "on":
        analyzed_count = 0
        for count in text1:
            analyzed_count = analyzed_count + 1

        params = {'purpose': 'Count chars ', 'analyzed_text': analyzed + ' Total count = ' + str(analyzed_count)}
        text1 = analyzed + ' Total count = ' + str(analyzed_count)

    if removepunc != "on" and fullcaps != "on" and newlineremover != "on" and spaceremover != "on" and charcount != "on":
        return HttpResponse("Please select at least one operation")
    return render(request, 'analyze.html', params)
